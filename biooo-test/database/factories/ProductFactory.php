<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

class ProductFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $goods = ['Table', 'Computer', 'Armchair', 'Dress', 'Meat', 'Coffee'];
        $where = ['from USA', 'from Germany', 'from Czech', 'from Ukraine', 'from China', 'from UAE'];
        $property = ['beautiful', 'amazing', 'tasty', 'aromatic', 'newest model', 'vintage'];
        $actions = ['discount', 'credit', 'action', 'sale', 'gross price', '+ gift', '2 for 1 price'];
        return [
            'product' => $this->faker->randomElement($goods) . ' ' . $this->faker->randomElement($where) . ' ' . $this->faker->randomElement($property) . ' ' . $this->faker->randomElement($actions),
            'description' => $this->faker->text,
            'price' => rand(12, 199),
            'vat_rate' => rand(1,20),
        ];
    }
}
