<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(LocaleSeeder::class);
        $this->call(CountrySeeder::class);
        $this->call(VatSeeder::class);
        $this->call(ProductSeeder::class);
    }
}
