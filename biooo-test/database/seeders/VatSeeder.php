<?php

namespace Database\Seeders;

use App\Models\Vat;
use Illuminate\Database\Seeder;

class VatSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $vats = range(1, 20);
        foreach ($vats as $vat) {
            Vat::create([
                'rate' => $vat,
            ]);
        }
        $this->command->info('VAT seeded successfully.');
    }
}
