<?php

namespace Database\Seeders;

use App\Models\Locale;
use Illuminate\Database\Seeder;

class LocaleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $locales = [
            [
                'locale' => 'English',
                'native' => 'English',
                'iso' => 'en',
            ],
            [
                'locale' => 'Arabic',
                'native' => 'العربية',
                'iso' => 'ar',
            ],
            [
                'locale' => 'Czech',
                'native' => 'čeština',
                'iso' => 'cs',
            ],
            [
                'locale' => 'Ukrainian',
                'native' => 'Українська',
                'iso' => 'uk',
            ],
            [
                'locale' => 'Japanese',
                'native' => '日本語 (にほんご)',
                'iso' => 'ja',
            ],
        ];

        foreach ($locales as $locale) {
            $newLocale = new Locale();
            $newLocale->locale = $locale['locale'];
            $newLocale->native = $locale['native'];
            $newLocale->iso = $locale['iso'];
            $newLocale->save();
        }

        $this->command->info('Locales seeded successfully.');
    }
}
