<?php

namespace Database\Seeders;

use App\Models\Country;
use Illuminate\Database\Seeder;

class CountrySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $countries = [
            [
                'country' => 'England',
            ],
            [
                'country' => 'Saudi Arabia',
            ],
            [
                'country' => 'Czech',
            ],
            [
                'country' => 'Ukraine',
            ],
            [
                'country' => 'Japan',
            ],
        ];

        foreach ($countries as $country) {
            $newCountry = new Country();
            $newCountry->country = $country['country'];
            $newCountry->save();
        }

        $this->command->info('countries seeded successfully.');
    }
}
