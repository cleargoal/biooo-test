<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreLocaleRequest;
use App\Http\Requests\UpdateLocaleRequest;
use App\Models\Locale;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Request;

class LocaleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Application|Factory|View
     */
    public function index(): Application|Factory|View
    {
        return view('locales.locales')->with(['locales' =>  Locale::all()]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Application|Factory|View
     */
    public function create(): View|Factory|Application
    {
        return view('locales.add-locale');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param StoreLocaleRequest $request
     * @return RedirectResponse
     */
    public function store(StoreLocaleRequest $request): RedirectResponse
    {
        $newLocale = new Locale();
        $newLocale->locale = $request['locale'];
        $newLocale->native = $request['native'];
        $newLocale->iso = $request['iso'];
        $newLocale->save($request->all());
        return redirect()->route('locales.index');
    }

    /**
     * Display the specified resource.
     *
     * @param Locale $locale
     * @return Application|Factory|View
     */
    public function show(Locale $locale): View|Factory|Application
    {
        return view('locales.show-locale')->with(['locale' => $locale]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Locale $locale
     * @return Application|Factory|View
     */
    public function edit(Locale $locale): View|Factory|Application
    {
        return view('locales.edit-locale')->with(['locale' => $locale]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param UpdateLocaleRequest $request
     * @param Locale $locale
     * @return RedirectResponse
     */
    public function update(UpdateLocaleRequest $request, Locale $locale): RedirectResponse
    {
        $locale->locale = $request['locale'];
        $locale->native = $request['native'];
        $locale->iso = $request['iso'];
        $locale->save($request->all());
        return redirect()->route('locales.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Locale $locale
     * @return RedirectResponse
     */
    public function destroy(Locale $locale): RedirectResponse
    {
        $locale->delete();
        return redirect()->route('locales.index');
    }

    /** Set Locale
     *
     * @param
     * @return RedirectResponse
     */
    public function setLocale($locale): RedirectResponse
    {
        App::setLocale($locale);
        session()->put('locale', $locale);
        return redirect()->back();
    }

}
