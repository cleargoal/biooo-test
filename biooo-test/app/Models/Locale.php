<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Locale extends Model
{
    use HasFactory;

    /**
     * @var mixed|string
     */
    private mixed $locale;
    /**
     * @var mixed|string
     */
    private mixed $native;
    /**
     * @var mixed|string
     */
    private mixed $iso;

    public $timestamps = false;

    protected $fillable = ['locale', 'native', 'iso',];

    protected array $rules = [
        'locale' => 'string|required|max:50',
        'native' => 'string|required|max:50',
        'iso' => 'string|unique:locales|required|max:2',
    ];

    /**
     * @param $value
     * @return mixed
     */
    public function getLocaleAttribute($value): mixed
    {
        return $value;
    }

    /**
     * @param mixed|string $locale
     */
    public function setLocaleAttribute(mixed $locale): void
    {
        $this->attributes['locale'] = $locale;
    }

    /**
     * @return mixed
     */
    public function getNativeAttribute($value): mixed
    {
        return $value;
    }

    /**
     * @param mixed|string $native
     */
    public function setNativeAttribute(mixed $native): void
    {
        $this->attributes['native'] = $native;
    }

    /**
     * @param $value
     * @return mixed
     */
    public function getIsoAttribute($value): mixed
    {
        return $value;
    }

    /**
     * @param mixed|string $iso
     */
    public function setIsoAttribute(mixed $iso): void
    {
        $this->attributes['iso'] = $iso;
    }

    public function countries(): \Illuminate\Database\Eloquent\Relations\BelongsToMany
    {
        return $this->belongsToMany(Country::class);
    }

}
