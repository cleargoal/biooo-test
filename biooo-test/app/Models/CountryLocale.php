<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CountryLocale extends Model
{
    use HasFactory;

    public $timestamps = false;
    protected $fillable = ['country_id', 'locale_id'];

}
