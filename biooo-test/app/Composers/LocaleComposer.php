<?php

namespace App\Composers;

use App\Models\Locale;
use Illuminate\View\View;

class LocaleComposer
{
    /**
     * Bind data to the view.
     *
     * @param View $view
     * @return void
     */
    public function compose(View $view)
    {
        $view->with('localesList', Locale::all());
    }
}
