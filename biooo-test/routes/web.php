<?php

use Illuminate\Foundation\Application;
use Illuminate\Support\Facades\Route;
use Inertia\Inertia;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::middleware(['localize'])->group(function () {
//Route::middleware([])->group(function () {
    Route::get('/', function () {
        return view('/welcome');
    });
    Route::get('/dashboard', function () {
        return view('/dashboard');
    })->name('dashboard');

    Route::resource('/locales', \App\Http\Controllers\LocaleController::class);
    Route::get('/setlocale/{locale}', [\App\Http\Controllers\LocaleController::class, 'setLocale'])->name('set.locale');
    Route::resource('/countries', \App\Http\Controllers\CountryController::class);
    Route::resource('/products', \App\Http\Controllers\ProductController::class);
    Route::resource('/vats', \App\Http\Controllers\VatController::class);
});
require __DIR__ . '/auth.php';
