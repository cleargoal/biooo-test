<p align="center"><a href="https://laravel.com" target="_blank"><img src="https://raw.githubusercontent.com/laravel/art/master/logo-lockup/5%20SVG/2%20CMYK/1%20Full%20Color/laravel-logolockup-cmyk-red.svg" width="400"></a></p>

<p align="center">
<a href="https://travis-ci.org/laravel/framework"><img src="https://travis-ci.org/laravel/framework.svg" alt="Build Status"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://img.shields.io/packagist/dt/laravel/framework" alt="Total Downloads"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://img.shields.io/packagist/v/laravel/framework" alt="Latest Stable Version"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://img.shields.io/packagist/l/laravel/framework" alt="License"></a>
</p>

## How to install test project

### preferably use Linux OS

### Open your system terminal and run commands

- *(git clone git@gitlab.com:cleargoal/biooo-test.git)*
##### since you see this document, it means that you already have a clone of the project 
- copy file '.env.example' to '.env'; it's prepared for using docker containers

#### Also, it's comfortable to add next command
-         alias sail='[ -f sail ] && bash sail || bash vendor/bin/sail'
to your file .bashrc for example before part named 'Alias definitions.'
It allows you to start all commands with just 'sail', not with './vendor/bin/sail'.
But it's not obligated. If not edit .bashrc, so in followed commands replace 'sail' with ./vendor/bin/sail.

-         sail up -d
-         sail composer i
-         sail npm install
-         sail npm run dev
-         sail artisan migrate --seed

Now you can start project in browser in path http://localhost:8012
