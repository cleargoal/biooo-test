<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('You CAN delete Locale ') . $locale->locale }}
        </h2>
    </x-slot>

    <div class="py-4">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
                <div class="p-2 bg-white border-b border-gray-200" style="width:40%;">
                    <form action="{{route('locales.destroy', ['locale' => $locale->id])}}" method="post"
                          class="flex flex-col">
                        @csrf
                        @method('delete')
                        <div type="text" class="mt-6">Locale Name: {{$locale->locale}} </div>

                        <div type="text" class="mt-6">Native Locale Name: {{$locale->native}}</div>

                        <div type="text" class="mt-6">Locale ISO Code: {{$locale->iso}}</div>

                        <h2 class="mt-10 font-extrabold text-5xl">Are you sure?</h2>
                        <input type="submit" class="mt-6 py-6" value="SURE!">
                        <input type="button" class="mt-6 py-6" value="Sorry..."
                               onclick="window.location.href='{{route('locales.index')}}'">
                    </form>
                </div>
            </div>
        </div>
    </div>
</x-app-layout>
