<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Countries') }} <span class="ml-4"><x-nav-link :href="route('countries.create')">{{ __('Add') }} {{ __('country') }}</x-nav-link></span>
        </h2>
    </x-slot>

    <div class="py-4">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
                <div class="p-2 bg-white border-b border-gray-200">
                    <table class="table table-auto">
                        <thead>
                        <tr class="table-row table-header-group font-extrabold text-2xl">
                            <td class="table-cell p-3">{{ __('Country') }}</td>
                            <td class="table-cell p-3">{{ __('Language') }}</td>
                            <td class="table-cell p-3">{{ __('VAT') }} {{ __('rate') }}</td>
                            <td class="table-cell p-3">{{ __('Operations') }}</td>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($countries as $country)
                            <tr>
                                <td class=" p-3">{{$country->country}}</td>
                                <td class=" p-3">{{$country->locales[0]->native}}</td>
                                <td class=" p-3">{{$country->iso}}</td>
                                <td class=" p-3 flex place-content-between">
                                    <a href="{{route('countries.edit', ['country' => $country])}}">{{ __('Edit') }}</a>
                                    <a href="{{route('countries.show', ['country' => $country])}}">{{ __('Delete') }}</a>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</x-app-layout>
