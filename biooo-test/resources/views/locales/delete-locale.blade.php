<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Add Locale') }}
        </h2>
    </x-slot>

    <div class="py-4">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
                <div class="p-2 bg-white border-b border-gray-200" style="width:40%;">
{{--                    <x-auth-validation-errors class="mb-4" :errors="$errors" />--}}
                    <form action="{{route('locales.store')}}" method="post" class="flex flex-col">
                        @csrf
                        <input type="text" class="mt-6" name="locale" placeholder="Enter Locale Name" value="{{old('locale')}}" required autofocus>
                        <div class="text-sm text-purple-800">@error('locale'){{$message}}@enderror</div>

                        <input type="text" class="mt-6" name="native" placeholder="Enter Native Locale Name" value="{{old('native')}}" required>
                        <div class="text-sm text-purple-800">@error('native'){{$message}}@enderror</div>

                        <input type="text" class="mt-6" name="iso" placeholder="Enter Locale ISO Code" value="{{old('iso')}}" maxlength="2" required>
                        <div class="text-sm text-purple-800">@error('iso'){{$message}}@enderror</div>
                        <input type="submit" class="mt-6 py-6">
                    </form>
                </div>
            </div>
        </div>
    </div>
</x-app-layout>
