<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Locales') }} <span class="ml-4"><x-nav-link :href="route('locales.create')">Add locale</x-nav-link></span>
        </h2>
    </x-slot>

    <div class="py-4">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
                <div class="p-2 bg-white border-b border-gray-200">
                    <table class="table table-auto">
                        <thead>
                        <tr class="table-row table-header-group font-extrabold text-2xl">
                            <td class="table-cell p-3">Locale</td>
                            <td class="table-cell p-3">Native</td>
                            <td class="table-cell p-3">ISO</td>
                            <td class="table-cell p-3">Operations</td>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($locales as $locale)
                            <tr>
                                <td class=" p-3">{{$locale->locale}}</td>
                                <td class=" p-3">{{$locale->native}}</td>
                                <td class=" p-3">{{$locale->iso}}</td>
                                <td class=" p-3 flex place-content-between">
                                    <a href="{{route('locales.edit', ['locale' => $locale])}}">Edit</a>
                                    <a href="{{route('locales.show', ['locale' => $locale])}}">Delete</a>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</x-app-layout>
