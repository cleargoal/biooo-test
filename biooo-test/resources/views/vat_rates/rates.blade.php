<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('VAT') }} {{ __('rates') }} <span class="ml-4"><x-nav-link :href="route('vats.create')">{{ __('Add') }} {{ __('VAT') }} {{ __('rate') }}</x-nav-link></span>
        </h2>
    </x-slot>

    <div class="py-4">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
                <div class="p-2 bg-white border-b border-gray-200">
                    <table class="table table-auto">
                        <thead>
                        <tr class="table-row table-header-group font-extrabold text-2xl">
                            <td class="table-cell p-3">{{ __('VAT rate') }}</td>
                            <td class="table-cell p-3">{{ __('Description') }}</td>
                            <td class="table-cell p-3">{{ __('Price') }} {{ __('net') }}</td>
                            <td class="table-cell p-3">{{ __('VAT') }}</td>
                            <td class="table-cell p-3">{{ __('Price') }} {{ __('gross') }}</td>
                            <td class="table-cell p-3">{{ __('Operations') }}</td>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($vats as $vat)
                            <tr>
                                <td class=" p-3">{{$vat->vat}}</td>
                                <td class=" p-3">{{$vat->description}}</td>
                                <td class=" p-3">{{$vat->price}}</td>
                                <td class=" p-3">{{$vat->vat_rate}}</td>
                                <td class=" p-3">{{$vat->price * ($vat->vat_rate/100+1)}}</td>
                                <td class=" p-3 flex place-content-between">
                                    <a href="{{route('vats.edit', ['vat' => $vat])}}">{{ __('Edit') }}</a>
                                    <a href="{{route('vats.show', ['vat' => $vat])}}">{{ __('Delete') }}</a>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</x-app-layout>
